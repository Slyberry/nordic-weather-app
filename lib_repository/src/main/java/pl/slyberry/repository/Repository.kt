package pl.slyberry.repository

import pl.slyberry.domain.CurrentWeather
import pl.slyberry.domain.Location

interface Repository {

    suspend fun getLocationId(location: Location): String

    suspend fun getCurrentWeather(locationId: String): CurrentWeather
}
