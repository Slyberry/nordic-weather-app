package pl.slyberry.repository.mapper

import pl.slyberry.domain.CurrentWeather
import pl.slyberry.domain.Precipitation
import pl.slyberry.domain.Pressure
import pl.slyberry.domain.Wind
import pl.slyberry.repository.rest.*

internal fun WeatherJson.toDomain(): CurrentWeather {
    return CurrentWeather(
        WeatherText,
        WeatherIcon,
        Temperature.Metric.Value,
        ApparentTemperature.Metric.Value,
        createWind(Wind),
        createPressure(Pressure, PressureTendency),
        createPrecipitation(PrecipitationSummary),
        MobileLink
    )
}

private fun createWind(wind: WindJson): Wind {
    return Wind(
        wind.Direction.Degrees,
        wind.Direction.English,
        wind.Speed.Metric.Value,
        wind.Speed.Metric.Unit
    )
}

private fun createPressure(pressure: PressureJson, tendency: PressureTendencyJson): Pressure {
    return Pressure(pressure.Metric.Value, pressure.Metric.Unit, tendency.LocalizedText)
}

private fun createPrecipitation(summary: PrecipitationSummaryJson): List<Precipitation> {
    return listOf(
        Precipitation(summary.Precipitation.Metric.Value, summary.Precipitation.Metric.Unit, 0),
        Precipitation(summary.PastHour.Metric.Value, summary.PastHour.Metric.Unit, 1),
        Precipitation(summary.Past3Hours.Metric.Value, summary.Past3Hours.Metric.Unit, 3),
        Precipitation(summary.Past6Hours.Metric.Value, summary.Past6Hours.Metric.Unit, 6),
        Precipitation(summary.Past9Hours.Metric.Value, summary.Past9Hours.Metric.Unit, 9),
        Precipitation(summary.Past12Hours.Metric.Value, summary.Past12Hours.Metric.Unit, 12),
        Precipitation(summary.Past18Hours.Metric.Value, summary.Past18Hours.Metric.Unit, 18),
        Precipitation(summary.Past24Hours.Metric.Value, summary.Past24Hours.Metric.Unit, 24),
    )
}
