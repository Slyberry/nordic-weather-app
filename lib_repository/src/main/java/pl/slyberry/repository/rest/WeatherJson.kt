package pl.slyberry.repository.rest

internal data class WeatherJson(
    val WeatherText: String,
    val WeatherIcon: Int,
    val Temperature: TemperaturesJson,
    val Wind: WindJson,
    val Pressure: PressureJson,
    val PressureTendency: PressureTendencyJson,
    val ApparentTemperature: ApparentTemperatureJson,
    val PrecipitationSummary: PrecipitationSummaryJson,
    val MobileLink: String
)

internal data class TemperaturesJson(
    val Metric: MetricJson,
    val Imperial: MetricJson
)

internal data class MetricJson(
    val Value: Float,
    val Unit: String
)

internal data class WindJson(
    val Direction: DirectionJson,
    val Speed: SpeedJson
)

internal data class DirectionJson(
    val Degrees: Int,
    val English: String
)

internal data class SpeedJson(
    val Metric: MetricJson
)

internal data class PressureJson(
    val Metric: MetricJson
)

internal data class PressureTendencyJson(
    val LocalizedText: String
)

internal data class ApparentTemperatureJson(
    val Metric: MetricJson
)

internal data class PrecipitationSummaryJson(
    val Precipitation: PrecipitationJson,
    val PastHour: PrecipitationJson,
    val Past3Hours: PrecipitationJson,
    val Past6Hours: PrecipitationJson,
    val Past9Hours: PrecipitationJson,
    val Past12Hours: PrecipitationJson,
    val Past18Hours: PrecipitationJson,
    val Past24Hours: PrecipitationJson,
)

internal data class PrecipitationJson(
    val Metric: MetricJson
)