package pl.slyberry.repository

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import pl.slyberry.repository.internal.NetworkRepository
import pl.slyberry.repository.internal.RetrofitService
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "https://dataservice.accuweather.com/"
private const val API_KEY = "rMVz9ciscWrAbGYSIUn0voqK8C2bWkVb"

val repositoryModule = module {
    single<Repository> { NetworkRepository(get(), API_KEY) }
    single { createRetrofit(get()) }
    single { createService(get()) }
    single { createOkHttpClient() }
}

private fun createOkHttpClient(): OkHttpClient {
    val interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    return OkHttpClient.Builder().addInterceptor(interceptor).build()
}

private fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
}

private fun createService(retrofit: Retrofit): RetrofitService {
    return retrofit.create(RetrofitService::class.java)
}
