package pl.slyberry.repository.internal

import pl.slyberry.domain.Location

internal object LocationFormatter {

    fun formatToRequest(location: Location): String {
        val lat = String.format("%.2f", location.lat)
        val long = String.format("%.2f", location.long)
        return String.format("%s,%s", lat, long)
    }
}
