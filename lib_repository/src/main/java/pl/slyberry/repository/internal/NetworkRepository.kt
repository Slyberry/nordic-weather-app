package pl.slyberry.repository.internal

import pl.slyberry.domain.CurrentWeather
import pl.slyberry.domain.Location
import pl.slyberry.domain.Wind
import pl.slyberry.repository.Repository
import pl.slyberry.repository.mapper.toDomain
import pl.slyberry.repository.rest.WindJson

internal class NetworkRepository(
    private val service: RetrofitService,
    private val apiKey: String
) : Repository {

    override suspend fun getLocationId(location: Location): String {
        val request = LocationFormatter.formatToRequest(location)
        return service.getLocationKey(request, apiKey).Key
    }

    override suspend fun getCurrentWeather(locationId: String): CurrentWeather {
        val result = service.getCurrentWeather(locationId, apiKey).first()
        return result.toDomain()
    }
}
