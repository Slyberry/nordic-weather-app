package pl.slyberry.repository.internal

import pl.slyberry.repository.rest.LocationResponse
import pl.slyberry.repository.rest.WeatherJson
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface RetrofitService {

    @GET("locations/v1/cities/geoposition/search.json")
    suspend fun getLocationKey(@Query("q") location: String, @Query("apikey") apiKey: String): LocationResponse

    @GET("currentconditions/v1/{id}")
    suspend fun getCurrentWeather(
        @Path("id") id: String,
        @Query("apikey") apiKey: String,
        @Query("details") details: Boolean = true
    ) : List<WeatherJson>
}
