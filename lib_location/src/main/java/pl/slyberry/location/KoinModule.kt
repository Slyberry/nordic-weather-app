package pl.slyberry.location

import org.koin.dsl.module

val locationModule = module {
    factory<LocationProvider> { LocationProviderImpl(get()) }
}
