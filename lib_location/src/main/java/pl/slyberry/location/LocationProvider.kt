package pl.slyberry.location

import pl.slyberry.domain.Location

interface LocationProvider {

    suspend fun getCurrentLocation(): Location
}
