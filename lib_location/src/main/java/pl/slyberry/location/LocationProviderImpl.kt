package pl.slyberry.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import pl.slyberry.domain.Location
import java.util.concurrent.LinkedBlockingQueue

import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

internal class LocationProviderImpl(private val context: Context) : LocationProvider {

    @SuppressLint("MissingPermission")
    override suspend fun getCurrentLocation() = suspendCoroutine<Location> { continuation ->
        if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) && hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            throw MissingLocationPermissionException()
        }

        val executor = ThreadPoolExecutor(
            1, 1, 0L, TimeUnit.MILLISECONDS,
            LinkedBlockingQueue()
        )

        LocationServices.getFusedLocationProviderClient(context)
            .lastLocation
            .addOnSuccessListener(executor) {
                continuation.resume(Location(it.latitude, it.longitude))
            }
    }

    private fun hasPermission(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED
    }
}

class MissingLocationPermissionException : Throwable()
