package pl.slyberry.nordicweatherapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.slyberry.business.businessModule
import pl.slyberry.location.locationModule
import pl.slyberry.repository.repositoryModule

class NordicApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@NordicApplication)
            modules(
                mainModule,
                repositoryModule,
                businessModule,
                locationModule
            )
        }
    }
}
