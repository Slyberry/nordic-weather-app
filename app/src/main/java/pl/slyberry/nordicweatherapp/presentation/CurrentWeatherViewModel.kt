package pl.slyberry.nordicweatherapp.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.business.GetCurrentWeatherUseCase
import pl.slyberry.domain.CurrentWeather
import pl.slyberry.domain.ErrorResource
import pl.slyberry.domain.LoadingResource
import pl.slyberry.domain.SuccessResource
import pl.slyberry.location.MissingLocationPermissionException
import pl.slyberry.nordicweatherapp.common.exhaustive
import pl.slyberry.nordicweatherapp.view.*

class CurrentWeatherViewModel(private val getCurrentWeatherUseCase: GetCurrentWeatherUseCase) : ViewModel() {

    val state = MutableLiveData<CurrentWeatherState>(LoadingState)
    private var currentState: CurrentWeatherState
        get() = state.value!!
        set(value) { state.value = value }

    init {
        downloadCurrentWeather()
    }

    fun downloadCurrentWeather() {
        getCurrentWeatherUseCase.perform().onEach {
            when (it) {
                is LoadingResource -> handleLoading()
                is SuccessResource -> handleSuccess(it.data)
                is ErrorResource -> handleError(it.error)
            }.exhaustive
        }.launchIn(viewModelScope)
    }

    fun displayRationale() {
        currentState = PermissionBlockedState
    }

    private fun handleLoading() {
        currentState = LoadingState
    }

    private fun handleSuccess(data: CurrentWeather) {
        currentState = SuccessState(data)
    }

    private fun handleError(error: Throwable) {
        currentState = when (error) {
            is MissingLocationPermissionException -> MissingPermissionState
            else -> NetworkErrorState
        }
        error.printStackTrace()
    }
}
