package pl.slyberry.nordicweatherapp.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import pl.slyberry.nordicweatherapp.R
import pl.slyberry.nordicweatherapp.theme.NordicColors

@Composable
fun BlockedScreen(message: String, onClick: () -> Unit) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Box(modifier = Modifier
                .padding(16.dp)
                .clip(shape = RoundedCornerShape(10.dp))
                .background(NordicColors.SemiWhite)
                .padding(16.dp)
            ) {
                Text(
                    text = message,
                    color = NordicColors.Red,
                    textAlign = TextAlign.Center
                )
            }

            Button(modifier = Modifier.padding(32.dp), onClick = onClick) {
                Text(text = stringResource(id = R.string.retry))
            }
        }
    }
}

@Preview
@Composable
fun NetworkErrorScreenPreview() {
    HomeScreen(NetworkErrorState) { }
}
