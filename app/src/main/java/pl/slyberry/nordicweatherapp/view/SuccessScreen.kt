package pl.slyberry.nordicweatherapp.view

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.glide.GlideImage
import pl.slyberry.domain.CurrentWeather
import pl.slyberry.domain.Precipitation
import pl.slyberry.domain.Pressure
import pl.slyberry.domain.Wind
import pl.slyberry.nordicweatherapp.R
import pl.slyberry.nordicweatherapp.common.openWebBrowser
import pl.slyberry.nordicweatherapp.theme.NordicColors

@Composable
fun SuccessScreen(state: SuccessState, onClick: () -> Unit) {
    val context = LocalContext.current.applicationContext

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier.verticalScroll(ScrollState(0)),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CurrentWeatherSection(state)
            PressureSection(state.data.pressure)
            PrecipitationsSection(state.data.precipitations)
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 32.dp, vertical = 16.dp),
                onClick = { context.openWebBrowser(state.data.mobileUrl) }
            ) {
                Text(text = stringResource(id = R.string.open_in_web))
            }
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 32.dp, vertical = 16.dp),
                onClick = onClick
            ) {
                Text(text = stringResource(id = R.string.retry))
            }
        }
    }
}

@Composable
fun CurrentWeatherSection(state: SuccessState) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clip(shape = RoundedCornerShape(10.dp))
            .background(NordicColors.SemiWhite)
            .padding(top = 16.dp, bottom = 16.dp, end = 16.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                modifier = Modifier
                    .padding(bottom = 8.dp),
                fontWeight = FontWeight.Bold,
                text = state.data.description,
                color = NordicColors.DarkBlue,
                fontSize = 18.sp
            )
            Text(
                text = state.getTemperature(),
                color = state.getTemperatureColor(),
                fontSize = 24.sp
            )
        }
        GlideImage(
            modifier = Modifier
                .width(80.dp)
                .height(80.dp),
            data = state.data.iconUrl,
            contentDescription = "Weather icon"
        ) { }
    }
}

@Composable
fun PressureSection(pressure: Pressure) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clip(shape = RoundedCornerShape(10.dp))
            .background(NordicColors.SemiWhite)
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        LabelText(
            stringResource(id = R.string.pressure),
            pressure.displayValue
        )
        LabelText(
            stringResource(id = R.string.tendency),
            pressure.tendency
        )
    }
}

@Composable
fun LabelText(title: String, description: String) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = title,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            color = NordicColors.Orange
        )
        Text(
            text = description,
            fontSize = 18.sp,
            color = NordicColors.Yellow
        )
    }
}

@Composable
fun PrecipitationsSection(precipitations: List<Precipitation>) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clip(shape = RoundedCornerShape(10.dp))
            .background(NordicColors.SemiWhite)
            .padding(16.dp)
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp),
            textAlign = TextAlign.Start,
            text = stringResource(id = R.string.precipitations),
            fontWeight = FontWeight.Bold,
            color = NordicColors.DarkBlue,
            fontSize = 24.sp
        )
        precipitations.forEach { PrecipitationSectionItem(it) }
    }
}

@Composable
fun PrecipitationSectionItem(precipitation: Precipitation) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 8.dp), horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = precipitation.displayValue,
            color = NordicColors.DarkBlue,
            fontSize = 16.sp
        )
        Text(
            text = precipitation.displayTimeValue,
            color = NordicColors.Yellow,
            fontSize = 16.sp
        )
    }
}

@Preview
@Composable
fun SuccessScreenPreview() {
    val precipitation = Precipitation(12f, "mm", 0)
    val weather = CurrentWeather(
        "Mostly cloudy",
        1,
        12.3f,
        23f,
        Wind(23, "SE", 12f, "km/h"),
        Pressure(1011f, "mb", "Steady"),
        listOf(precipitation, precipitation, precipitation, precipitation),
        ""
    )
    HomeScreen(SuccessState(weather)) { }
}
