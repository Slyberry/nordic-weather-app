package pl.slyberry.nordicweatherapp.view

import androidx.compose.ui.graphics.Color
import pl.slyberry.domain.CurrentWeather
import pl.slyberry.nordicweatherapp.theme.NordicColors

sealed class CurrentWeatherState

object LoadingState: CurrentWeatherState()
object NetworkErrorState: CurrentWeatherState()
object MissingPermissionState: CurrentWeatherState()
object PermissionBlockedState: CurrentWeatherState()

data class SuccessState(val data: CurrentWeather): CurrentWeatherState() {

    fun getTemperature(): String {
        return String.format("%.2f °C", data.temperatureCelsius)
    }

    fun getTemperatureColor(): Color {
        val temperature = data.temperatureCelsius
        return when {
            temperature > 30 -> NordicColors.Red
            temperature > 20 -> NordicColors.Yellow
            temperature > 10 -> NordicColors.Green
            temperature > 0 -> NordicColors.Blue
            else -> NordicColors.LightBlue
        }
    }
}
