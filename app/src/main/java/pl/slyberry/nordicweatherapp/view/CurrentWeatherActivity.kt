package pl.slyberry.nordicweatherapp.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import org.koin.androidx.viewmodel.ext.android.viewModel
import pl.slyberry.nordicweatherapp.presentation.CurrentWeatherViewModel

private const val REQUEST_LOCATION = 111

class CurrentWeatherActivity : AppCompatActivity() {

    private val viewModel by viewModel<CurrentWeatherViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            HomeScreen(viewModel = viewModel)
        }

        viewModel.state.observe(this) {
            Log.d("LOG--TEST", it.toString())
            (it as? MissingPermissionState)?.let {
                requestLocationPermission()
            }
        }
    }

    private fun requestLocationPermission() {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_LOCATION && grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
            viewModel.downloadCurrentWeather()
        } else {
            viewModel.displayRationale()
        }
    }
}
