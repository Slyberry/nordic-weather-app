package pl.slyberry.nordicweatherapp.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import pl.slyberry.nordicweatherapp.R
import pl.slyberry.nordicweatherapp.common.exhaustive
import pl.slyberry.nordicweatherapp.presentation.CurrentWeatherViewModel
import pl.slyberry.nordicweatherapp.theme.NordicTheme

@Composable
fun HomeScreen(viewModel: CurrentWeatherViewModel) {

    val state = viewModel.state.observeAsState()

    state.value?.let {
        HomeScreen(it) { viewModel.downloadCurrentWeather() }
    }
}

@Composable
fun HomeScreen(state: CurrentWeatherState, updateRequired: () -> Unit)= NordicTheme {
    Box(modifier = Modifier.fillMaxSize()) {
        Image(
            modifier = Modifier.fillMaxSize(),
            painter = painterResource(id = R.drawable.blue_background),
            contentDescription = "",
            contentScale = ContentScale.FillWidth
        )
        when (state) {
            LoadingState -> LoadingScreen()
            MissingPermissionState -> MissingPermissionScreen()
            NetworkErrorState -> BlockedScreen(stringResource(id = R.string.network_error)) { updateRequired() }
            is SuccessState -> SuccessScreen(state) { updateRequired() }
            PermissionBlockedState -> BlockedScreen(stringResource(id = R.string.permission_denied)) { updateRequired() }
        }.exhaustive
    }
}
