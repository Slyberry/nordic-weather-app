package pl.slyberry.nordicweatherapp.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import pl.slyberry.nordicweatherapp.theme.NordicColors

@Composable
fun LoadingScreen() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.height(150.dp).width(150.dp),
            color = NordicColors.LightBlue,
            strokeWidth = 5.dp
        )
    }
}

@Preview
@Composable
fun LoadingScreenPreview() {
    HomeScreen(LoadingState) { }
}
