package pl.slyberry.nordicweatherapp.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import pl.slyberry.nordicweatherapp.R

val NordicFontFamily = FontFamily(
    Font(R.font.montserrat_bold),
    Font(R.font.shojumaru, FontWeight.W500),
    Font(R.font.shojumaru, FontWeight.Bold)
)
