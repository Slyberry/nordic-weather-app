package pl.slyberry.nordicweatherapp.theme

import androidx.compose.ui.graphics.Color

object NordicColors {
    val Blue = Color(0xFF00A9CE)
    val LightBlue = Color(0xFF6AD1E3)
    val DarkBlue = Color(0xFF0077C8)
    val Green = Color(0xFFD0DF00)
    val Yellow = Color(0xFFFFCD00)
    val Red = Color(0xFFEE2F4E)
    val Orange = Color(0xFFF58220)
    val SemiWhite = Color(0x99FFFFFF)
    val White = Color(0xFFFFFFFF)
    val Black = Color(0xFF000000)
}
