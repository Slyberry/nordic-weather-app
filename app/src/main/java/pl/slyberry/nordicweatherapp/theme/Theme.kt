package pl.slyberry.nordicweatherapp.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Typography
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

@Composable
fun NordicTheme(content: @Composable () -> Unit) {
    val lightTheme = lightColors(
        primary = NordicColors.DarkBlue,
        primaryVariant = NordicColors.DarkBlue,
        onPrimary = NordicColors.White,
        secondary = NordicColors.DarkBlue,
        secondaryVariant = NordicColors.DarkBlue,
        onSecondary = NordicColors.White,
        onSurface = NordicColors.Black,
        onBackground = NordicColors.Black,
        error = NordicColors.Red,
        onError = NordicColors.White
    )

    val typography = Typography(NordicFontFamily)

    MaterialTheme(colors = lightTheme, typography = typography, content = content)
}
