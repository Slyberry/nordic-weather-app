package pl.slyberry.nordicweatherapp.common

import android.content.Context
import android.content.Intent
import android.net.Uri

val Any.exhaustive
    get() = this

fun Context.openWebBrowser(url: String) {
    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    browserIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    startActivity(browserIntent)
}
