package pl.slyberry.nordicweatherapp

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.slyberry.nordicweatherapp.presentation.CurrentWeatherViewModel

val mainModule = module {
    viewModel { CurrentWeatherViewModel(get()) }
}
