# Nordic Weather App
 
## Description
The application allows to download current weather conditions based on location. Data is downloaded automatically on app startup.
 
The application is able to handle request location permission in runtime.
In case of connection problems or missing permission the proper information is displayed. Requests can be repeated by clicking on the "retry" button.
 
Success screens provide two buttons to open the weather screen in the browser and to refresh data.
 
## Main screen
Successfully downloaded data are displayed in three sections.
 
#### Temperature section
First section contains three elements:
 
1. Description of current weather.
2. Current temperature in Celsius degrees. Color of this item depends on the value. The higher the temperature the warmer the color.
3. Icon that is downloaded from the network.
 
#### Pressure section
Pressure section contains two elements:
 
1. Current pressure together with the unit.
2. Tendency to change of the pressure.
 
#### Precipitations
This section contains current and historical data of precipitations. Data contains values for now, 1 hour before, 3h before, 6h, 9h, 12h, 18 and 24h before. Data set is fixed.
 
## Main screen
Application contains 5 Android modules. One application module and four library modules.
 
#### app
Main module which contains application definition and main screen. View and presentation layer are included. It contains every view together with a view model which uses a use case to download data.
 
#### lib_businses
Module with business logic. In this case it contains only one use case which gets current location and uses it to download current weather.
 
#### lib_location
Contains a provider which allows to download current location if necessary permissions are granted. Serves as a wrapper.
 
#### lib_repository
Provided access to data in a simple get/set manner. It shouldn't include any complex logic. It downloads data and maps it to domain objects.
 
#### lib_domain
Contains domain objects - plain data classes. It is used by every other module.
