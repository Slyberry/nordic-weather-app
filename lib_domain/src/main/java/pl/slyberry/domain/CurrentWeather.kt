package pl.slyberry.domain

data class CurrentWeather(
    val description: String,
    val iconId: Int,
    val temperatureCelsius: Float,
    val apparentTemperatureCelsius: Float,
    val wind: Wind,
    val pressure: Pressure,
    val precipitations: List<Precipitation>,
    val mobileUrl: String
) {

    val iconUrl = "https://developer.accuweather.com/sites/default/files/${getIconId()}-s.png"

    private fun getIconId() = if (iconId < 10) "0$iconId" else iconId
}

data class Wind(
    val degrees: Int,
    val direction: String,
    val speed: Float,
    val unit: String
)

data class Pressure(
    val pressure: Float,
    val unit: String,
    val tendency: String
) {

    val displayValue = String.format("%.2f %s", pressure, unit)
}

data class Precipitation(val amount: Float, val unit: String, val pastInTimeInHours: Int) {

    val displayValue = String.format("%.2f %s", amount, unit)

    val displayTimeValue = String.format("-%dh", pastInTimeInHours)
}
