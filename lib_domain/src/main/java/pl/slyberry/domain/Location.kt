package pl.slyberry.domain

data class Location(val lat: Double, val long: Double)
