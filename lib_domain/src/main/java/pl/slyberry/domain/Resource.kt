package pl.slyberry.domain

sealed class Resource <T>

class LoadingResource<T> : Resource<T>()
data class SuccessResource<T>(val data: T) : Resource<T>()
data class ErrorResource<T>(val error: Throwable) : Resource<T>()
