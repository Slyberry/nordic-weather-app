package pl.slyberry.business

import pl.slyberry.domain.CurrentWeather
import pl.slyberry.location.LocationProvider
import pl.slyberry.repository.Repository

class GetCurrentWeatherUseCase(
    private val repository: Repository,
    private val locationProvider: LocationProvider
) : DefaultUseCase<CurrentWeather>() {

    override suspend fun performTask(): CurrentWeather {
        val location = locationProvider.getCurrentLocation()
        val locationId = repository.getLocationId(location)
        return repository.getCurrentWeather(locationId)
    }
}
