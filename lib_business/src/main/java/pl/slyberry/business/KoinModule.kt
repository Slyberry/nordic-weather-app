package pl.slyberry.business

import org.koin.dsl.module

val businessModule = module {
    factory { GetCurrentWeatherUseCase(get(), get()) }
}
