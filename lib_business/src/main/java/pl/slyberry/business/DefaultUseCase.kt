package pl.slyberry.business

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import pl.slyberry.domain.ErrorResource
import pl.slyberry.domain.LoadingResource
import pl.slyberry.domain.Resource
import pl.slyberry.domain.SuccessResource

abstract class DefaultUseCase<Output> {

    fun perform(): Flow<Resource<Output>> {
        return flow {
            emit(LoadingResource())
            val result = withContext(Dispatchers.IO) { SuccessResource(performTask()) }
            emit(result)
        }.catch {
            emit(ErrorResource(it))
        }
    }

    protected abstract suspend fun performTask(): Output
}